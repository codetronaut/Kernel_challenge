// SPDX-License-Identifier: GPL-2.0
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/debugfs.h>
#include <linux/jiffies.h>

MODULE_DESCRIPTION("Task 08: debugfs");
MODULE_AUTHOR("Anmol Karn <anmol.karan123@gmail.com>");
MODULE_LICENSE("GPL");

static char *debugfs_id = "783dbec8fa0b";
static char message[32] = { 0 };

static struct dentry *dir;

static ssize_t debugfs_id_read(struct file *f, char __user *buf, size_t len,
			       loff_t *offset)
{
	return simple_read_from_buffer(buf, len, offset, debugfs_id,
				       strlen(debugfs_id));
}

static ssize_t debugfs_id_write(struct file *f, const char __user *buf,
				size_t len, loff_t *offset)
{
	int ret;

	ret = simple_write_to_buffer(message, sizeof(message), offset, buf, len);
	if (ret < 0)
		return ret;
	if (len - 1 != strlen(debugfs_id)
	    || strncmp(message, debugfs_id, strlen(debugfs_id)))
		return -EINVAL;
	return len;
}

static const struct file_operations debugfs_id_fops = {
	.owner = THIS_MODULE,
	.read = debugfs_id_read,
	.write = debugfs_id_write
};

int __init load_method(void)
{
	/* NULL mean it's path will be /sys/kernel/debug/ */
	dir = debugfs_create_dir("eudyptula", NULL);
	debugfs_create_file("id", 0666, dir, NULL, &debugfs_id_fops);
	debugfs_create_u32("jiffies", 0444, dir, (u32 *) & jiffies);
	pr_debug("Files created successfully!");

	return 0;
}

void __exit unload_method(void)
{
	debugfs_remove_recursive(dir);
	pr_debug("removing and cleaning...");
}

module_init(load_method);
module_exit(unload_method);
