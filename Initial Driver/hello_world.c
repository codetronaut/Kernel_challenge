/*
 * Author: Anmol Karn
 * hello_world.c
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>

static int load_method(void)
{
	pr_debug("Hello, World  kernel :) \n");
	return 0;
}

static void unload_method(void)
{
	pr_debug("unloading...\n");
}

module_init(load_method);
module_exit(unload_method);

MODULE_DESCRIPTION("My first kernel module for eudyptula challenge");
MODULE_AUTHOR(" Anmol ");
MODULE_LICENSE(" GPL ");
