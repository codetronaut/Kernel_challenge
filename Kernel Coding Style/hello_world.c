// SPDX-License-Identifier: GPL-2.0
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>

MODULE_DESCRIPTION("My first kernel module for eudyptula challenge.");
MODULE_AUTHOR("Anmol");
MODULE_LICENSE("GPL");

static int load_method(void)
{
	pr_debug("Hello, World kernel :)\n");
	return 0;
}

static void unload_method(void)
{
	pr_debug("unloading...\n");
}

module_init(load_method);
module_exit(unload_method);
