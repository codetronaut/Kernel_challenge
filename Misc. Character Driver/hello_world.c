// SPDX-License-Identifier: GPL-2.0
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/string.h>
#include <linux/fs.h>

#include <linux/uaccess.h>

MODULE_DESCRIPTION("Misc character driver, simple operation.");
MODULE_AUTHOR("Anmol Karn <anmol.karan123@gmail.com>");
MODULE_LICENSE("GPL");

static char *misc_char_id = "783dbec8fa0b";
static char message[32] = { 0 };

/* read function called whenever device is being read from userspace. */
static ssize_t misc_char_read(struct file *f, char __user *buf, size_t len,
			      loff_t *offset)
{
	return simple_read_from_buffer(buf, len, offset, misc_char_id,
				       strlen(misc_char_id));
}

/*write function called whenever device is being written to from user. */
static ssize_t misc_char_write(struct file *f, const char __user *buf,
			       size_t len, loff_t *offset)
{
	int ret;

	ret = simple_write_to_buffer(message, sizeof(message), offset, buf, len);
	if (ret < 0)
		return ret;
	if (len - 1 != strlen(misc_char_id)
	    || strncmp(message, misc_char_id, strlen(misc_char_id)))
		return -EINVAL;
	return len;
}

/* structure for the misc character driver initialization */
static const struct file_operations misc_char_fops = {
	.owner = THIS_MODULE,
	.read = misc_char_read,
	.write = misc_char_write
};

/* Dynamically set minor number */
static struct miscdevice misc_char_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "eudyptula",
	.fops = &misc_char_fops,
};

int __init load_method(void)
{
	int ret;

	ret = misc_register(&misc_char_device);
	if (ret)
		pr_debug("Can't register the misc char driver!");
	pr_debug("It's inside and Registered!");

	return ret;
}

void __exit unload_method(void)
{
	misc_deregister(&misc_char_device);
	pr_debug("It's exiting and unregistering!");
}

module_init(load_method);
module_exit(unload_method);
