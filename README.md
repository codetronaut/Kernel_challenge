# Linux kernel Hacking

This Project involves the various aspects to interact with the Linux Kernel and it's internals. It also contains Kernel Drivers for playing with different components.

### Prerequisites
The basic requirements are:

* A C compiler (defaults to gcc, can use llvm clang but then you need to use different options)

* make

* All other requirements can be found [here](https://www.kernel.org/doc/html/v4.10/process/changes.html). 


## Getting Started

#### Git Clone

First we need to check out the git repo:

```bash
❯ cd ~/<any_directory_present_in_this_directory>
❯ make <choose_options_present_in_Makefile>
```

## Author

Anmol (<anmol.karan123@gmail.com>)

## Contributing

Feel free to contribute :)
