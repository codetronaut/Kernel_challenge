// SPDX-License-Identifier: GPL-2.0
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/jiffies.h>
#include <linux/string.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/sysfs.h>
#include <linux/kobject.h>

MODULE_DESCRIPTION("Task 09: sysfs");
MODULE_AUTHOR("Anmol Karn <anmol.karan123@gmail.com>");
MODULE_LICENSE("GPL v2");

static char *sysfs_id = "783dbec8fa0b";
static DEFINE_MUTEX(foo_key);
static char sysfs_foo_msg[PAGE_SIZE];
static struct kobject *sysfs_dir;
/* foo file */
static ssize_t foo_show(struct kobject *kobj, struct kobj_attribute *attr,
			char *buf)
{
	int res;

	mutex_lock(&foo_key);
	res = sprintf(buf, "%s\n", sysfs_foo_msg);
	mutex_unlock(&foo_key);
	return res;
}

static ssize_t foo_store(struct kobject *kobj, struct kobj_attribute *attr,
			 const char *buf, size_t len)
{
	if (len >= PAGE_SIZE)
		return -EINVAL;

	mutex_lock(&foo_key);
	strncpy(sysfs_foo_msg, buf, len);
	mutex_unlock(&foo_key);

	return len;
}

static struct kobj_attribute foo_attribute =
__ATTR_RW(foo);

/* id file */
static ssize_t id_show(struct kobject *kobj, struct kobj_attribute *attr,
		       char *buf)
{
	return sprintf(buf, "%s\n", sysfs_id);
}

static ssize_t id_store(struct kobject *kobj, struct kobj_attribute *attr,
			const char *buf, size_t len)
{
	if (len - 1 != strlen(sysfs_id)
	    || strncmp(buf, sysfs_id, strlen(sysfs_id)))
		return -EINVAL;
	return len;
}

static struct kobj_attribute id_attribute = __ATTR_RW(id);

/* jiffies file */
static ssize_t jiffies_show(struct kobject *kobj, struct kobj_attribute *attr,
			    char *buf)
{
	return sprintf(buf, "%ld\n", jiffies);
}

static struct kobj_attribute jiffies_attribute = __ATTR_RO(jiffies);

static struct attribute *attrs[] = {
	&foo_attribute.attr,
	&id_attribute.attr,
	&jiffies_attribute.attr,
	NULL,
};

static struct attribute_group attr_group = {
	.attrs = attrs,
};

static int __init load_method(void)
{
	int res;

	sysfs_dir = kobject_create_and_add("eudyptula", kernel_kobj);
	if (IS_ERR(sysfs_dir))
		return -ENOMEM;
	res = sysfs_create_group(sysfs_dir, &attr_group);
	if (res)
		kobject_put(sysfs_dir);
	pr_debug("Successfully registered!");

	return res;
}

static void __exit unload_method(void)
{
	kobject_put(sysfs_dir);
	pr_debug("Unregistering the module...");
}

module_init(load_method);
module_exit(unload_method);
