  - Take the kernel module you wrote for task 01, and modify it so that
    when any USB keyboard is plugged in, the module will be
    automatically loaded by the correct userspace hotplug tools (which
    are implemented by depmod / kmod / udev / mdev / systemd, depending
    on what distro you are using.)

Need to set MODULE_DEVICE_TABLE so that hotplug knows to to call this module

    /* table of devices that work with this driver */
    static struct usb_device_id skel_table [] = {
     { USB_INTERFACE_INFO(TODO_class, TODO_subclass, TODO_protocol) },
     { }        /* Terminating entry */
    };

    MODULE_DEVICE_TABLE(usb, skel_table);

depmod reads MODULE_DEVICE_TABlE
  -> creates map file so hotplug scripts know which module to call 


device is plugged in
 -> /sbin/hotplug is called
  -> scripts under /etc/hotplug are read and called? (probably udev)
   -> hotplug scripts read module map files and load appropriate modules

From uapi/linux/hid.h:

USB_INTERFACE_CLASS_HID
USB_INTERFACE_SUBCLASS_BOOT
USB_INTERFACE_PROTOCOL_KEYBOARD

```
static struct usb_device_id kbd_table[] = {
        { USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID, USB_INTERFACE_SUBCLASS_BOOT, USB_INTERFACE_PROTOCOL_KEYBOARD) },
        { }
};

MODULE_DEVICE_TABLE(usb, kbd_table);
```


Method of completing the task:

1) create module hello_world.c
2) modify Makefile to print debug messages in the buffer.
3) run ```make``` 
4) copy hello_world.ko file in ```/lib/module/$(uname -r)/``` 
5) then run ```sudo depmod -a``` in the folder where modules exist. This will probe all files.
6) then run ```lsmod | grep hello_world ``` without connecting keyboard.
7) again run previous command after connecting keyboard.
8) check logs using ```journalctl -f```.
9) run ```sudo rmmod hello_world``` to unload the module.
10) done!





