// SPDX-License-Identifier: GPL-2.0
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/hid.h>
#include <linux/usb/input.h>
#include <linux/usb.h>

MODULE_DESCRIPTION("Implementation of hotplug USB autoreload module.");
MODULE_AUTHOR("Anmol");
MODULE_LICENSE("GPL");

/* usb device driver */
static struct usb_device_id usbk_id_table[] = {
	{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID,
			     USB_INTERFACE_SUBCLASS_BOOT,
			     USB_INTERFACE_PROTOCOL_KEYBOARD) },
	{ }
};

MODULE_DEVICE_TABLE(usb, usbk_id_table);

/* Module Definition */
/* load_method called on module loading */
static int load_method(void)
{
	pr_debug("usb keyboard plugged in!");
}

/* usb keyboard exit!  unload_method called */
static void unload_method(void)
{
	pr_debug("usb keyboard removed! module exiting...\n");
}

module_init(load_method);
module_exit(unload_method);
