#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include<linux/usb.h>

#define USBK_VENDOR_ID  0000
#define USBK_PRODUCT_ID 0538
#define USBK_MOD_NAME   "USB OPTICAL MOUSE"
#define USBK_DRV_NAME   "USB OPTICAL MOUSE"



//probe-- when usb connects...
static int usbk_probe(struct usbk_interface *interface, const struct usbk_device_id *id)
{   
	printk(KERN_NOTICE "USB k/b (%04X: %04X) plugged in\n", id->idVendor, id->idProduct);
	return 0;
}

static void usbk_disconnect(struct usbk_interface *interface)
{
    printk(KERN_NOTICE "%s:, ledbage USB device removed", USBK_DRV_NAME);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static struct usb_device_id usbk_table [] = {
    {USB_DEVICE(USBK_VENDOR_ID,USBK_PRODUCT_ID)},
    { }, // terminating entry
};
MODULE_DEVICE_TABLE(usb, usbk_table);


static struct usb_driver usbk_driver = {
    .name = USBK_DRV_NAME,
    .id_table = usbk_table,
    .probe = usbk_probe,
    .disconnect = usbk_disconnect,
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static int usbk_init(void)
{
    int ret = -1;

    printk(KERN_NOTICE "%s:, Loading module. Handler: %s",
             USBK_MOD_NAME, __FUNCTION__);

    printk(KERN_NOTICE "%s:, Attempting to register USB device.", USBK_MOD_NAME);

    // attempt to register usb device
    ret = usb_register(&usbk_driver);

    // log dmesg of status
    if(ret == 0) printk(KERN_NOTICE "%s:, Device registered!", USBK_MOD_NAME);
    else         printk(KERN_ERR    "%s:, Device registration failed > %d", USBK_MOD_NAME, ret);

    return ret;
    
}

static void usbk_exit(void)
{
	    printk(KERN_NOTICE "%s:, Unloading module. Handler: %s",
             USBK_MOD_NAME, __FUNCTION__);

    printk(KERN_NOTICE "%s:, Attempting to deregister USB device.", USBK_MOD_NAME);

    // attempt to deregister usb device
    usb_deregister(&usbk_driver);

    // log dmesg of status
    printk(KERN_NOTICE "%s:, Leaving module!", USBK_MOD_NAME);

}

module_init(usbk_init);
module_exit(usbk_exit);

MODULE_DESCRIPTION("My first kernel module for eudyptula challenge");
MODULE_AUTHOR(" Anmol ");
MODULE_LICENSE(" GPL ");

