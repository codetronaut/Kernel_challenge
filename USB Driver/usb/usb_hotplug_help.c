#include <linux/init.h>
#include <linux/module.h>
#include <linux/usb.h> 



#define LEDBADGE_VENDOR_ID  0x0483
#define LEDBADGE_PRODUCT_ID 0x5750
#define LEDBADGE_MOD_NAME   "ledbadge"
#define LEDBADGE_DRV_NAME   "ledbadge"


/************* usb operations *************/

/* 
    ledbadge_probe

        called when plugging the USB device */


static int ledbadge_probe(struct usb_interface *interface, const struct usb_device_id *id)
{   
    printk(KERN_NOTICE "Led Badge driver (%04X: %04X) plugged \n", id->idVendor, id->idProduct);
    return 0;
}


/* 
    ledbadge_disconnect

        called when unplugging the USB device */

static void ledbadge_disconnect(struct usb_interface *interface) 
{
    printk(KERN_NOTICE "%s:, ledbage USB device removed", LEDBADGE_DRV_NAME);
}




/************* usb driver *************/

// information required to recognize the device

static struct usb_device_id ledbadge_table [] = {
    {USB_DEVICE(LEDBADGE_VENDOR_ID, LEDBADGE_PRODUCT_ID)},
    { }, // terminating entry
};
MODULE_DEVICE_TABLE(usb, ledbadge_table);


static struct usb_driver ledbadge_driver = {
    .name = LEDBADGE_DRV_NAME,
    .id_table = ledbadge_table,
    .probe = ledbadge_probe,
    .disconnect = ledbadge_disconnect,
};








/************** module definition **************/

/* ledbadge_init

        caled on module loading */

static int __init ledbadge_init(void) 
{
    int ret = -1; 

    printk(KERN_NOTICE "%s:, Loading module. Handler: %s",
             LEDBADGE_MOD_NAME, __FUNCTION__); 

    printk(KERN_NOTICE "%s:, Attempting to register USB device.", LEDBADGE_MOD_NAME); 

    // attempt to register usb device
    ret = usb_register(&ledbadge_driver);

    // log dmesg of status
    if(ret == 0) printk(KERN_NOTICE "%s:, Device registered!", LEDBADGE_MOD_NAME);
    else         printk(KERN_ERR    "%s:, Device registration failed > %d", LEDBADGE_MOD_NAME, ret);

    return ret; 
}

/* ledbadge_exit

        caled on module unloading */

static void __exit ledbadge_exit(void) 
{
    printk(KERN_NOTICE "%s:, Unloading module. Handler: %s",
             LEDBADGE_MOD_NAME, __FUNCTION__); 

    printk(KERN_NOTICE "%s:, Attempting to deregister USB device.", LEDBADGE_MOD_NAME); 

    // attempt to deregister usb device
    usb_deregister(&ledbadge_driver);

    // log dmesg of status
    printk(KERN_NOTICE "%s:, Leaving module!", LEDBADGE_MOD_NAME);
}

module_init(ledbadge_init);
module_exit(ledbadge_exit); 

MODULE_LICENSE("GPL"); 
