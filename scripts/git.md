# Creating a User-Specific Git Configuration File
Let's start by configuring global git options, and then you can go on to cloning the kernel repository.

Create a user-specific Git configuration file named .gitconfig in your home directory with your name, email and other needed configurations. This information is used for commits and patch generation.

The email in the .gitconfig file should be the same email you will use to send patches. The name is the Author name, and the email is the email address for the commit. Linux kernel developers will not accept a patch where the From email differs from the Signed-off-by line, which is what will happen if these two emails do not match. Configuring signoff = true as shown above adds the Signed-off-line with the configured email as shown above in email=your.email@example.com to the commit. This can be done manually by running the git command with the -s option. E.g.:

git commit -s

Configure the name= field with your full legal name. I mentioned this earlier that by adding your Signed-off-by line to a patch, you are certifying that you have read and understood the Developer's Certificate of Origin and abide by the Linux Kernel Enforcement Statement. Please review the documents before you send patches to the kernel.


# Kernel Configuration
Let's work with the mainline kernel to create your first patch. By this time, if you completed the exercises from the previous chapters, you should already have the mainline kernel running on your system. While doing that, I asked you to copy the distribution configuration file to generate the kernel configuration. Now, let's talk about the kernel configuration.

The Linux kernel is completely configurable. Drivers can be configured to be installed and completely disabled. Here are three options for driver installation:

Disabled
Built into the kernel (vmlinux image) to be loaded at boot time
Built as a module to be loaded as needed using modprobe.
It is a good idea to configure drivers as modules, to avoid large kernel images. Modules (.ko files) can be loaded when the kernel detects hardware that matches the driver. Building drivers as modules allows them to be loaded on demand, instead of keeping them around in the kernel image even when the hardware is either not being used, or not even present on the system.

We talked about generating the new configuration with the old configuration as the starting point. New releases often introduce new configuration variables and, in some cases, rename the configuration symbols. The latter causes problems, and make oldconfig might not generate a new working kernel.

Run make listnewconfig after copying the configuration from /boot to the .config file, to see a list of new configuration symbols. Kconfig make config is a good source about Kconfig and make config. Please refer to the Kernel Build System to understand the kernel build framework and the kernel makefiles.

# Creating a New Branch
Before making a change, let's create a new branch in the linux_mainline repository you cloned earlier to write your first patch. We will start by adding a remote first to do a rebase (pick up new changes made to the mainline).

cd linux_mainline
git branch -a
* master
  remotes/linux/master
  remotes/origin?HEAD -> origin/master
  remotes/origin/master


# Adding a Remote
Let’s add git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git as the remote named linux. Adding a remote helps us fetch changes and choose a tag to rebase from.

git remote add linux git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
git fetch linux
remote: Counting objects: 3976, done.
remote: Compressing objects: 100% (1988/1988), done.
remote: Total 3976 (delta 2458), reused 2608 (delta 1969)
Receiving objects: 100% (3976/3976), 6.67 MiB | 7.80 MiB/s, done.
Resolving deltas: 100% (2458/2458), done.
From git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux
   2a11c76e5301..ecb095bff5d4   master -> linux/master
 * [new tag]                  v5.3-rc3 -> v5.3-rc3

We can pick a tag to rebase to. In this case, there is only one new tag. Let’s hold off on the rebase and start writing a new patch.



# Checkout the Branch
To check out a branch do:

git checkout -b work
master
* work
  remotes/linux/master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master


# Making Changes to a Driver
Now, let’s select a driver to make a change. Run lsmod to see the modules loaded on your system, and pick a driver to change. I will walk you through changing the uvcvideo driver. If you don’t have uvcvideo on your system, find a different driver and follow along using your driver name instead of uvcvideo.

Once you have the name of a driver, it's time to find out where the .c and .h files for that driver are in the Linux kernel repository. Even though searching Makefiles will get you the desired result, git grep will get you there faster, searching only the checked-in files in the repository. git grep will skip all generated files such as .o’s, .ko’s and binaries. It will skip the .git directory as well. Okay, now let’s run git grep to look for uvcvideo files.

git grep uvcvideo -- '*Makefile'
drivers/media/usb/uvc/Makefile:uvcvideo-objs := uvc_driver.o uvc_queue.o uvc_v4l2.o uvc_video.o uvc_ctrl.o drivers/media/usb/uvc/Makefile:uvcvideo-objs += uvc_entity.o
drivers/media/usb/uvc/Makefile:obj-$(CONFIG_USB_VIDEO_CLASS) += uvcvideo.o

uvcvideo is a USB Video Class (UVC) media driver for video input devices, such as webcams. It supports webcams on laptops. Let’s check the source files for this driver.

ls drivers/media/usb/uvc/
Kconfig uvc_debugfs.c uvc_isight.c uvc_status.c uvcvideo.h
Makefile uvc_driver.c uvc_metadata.c uvc_v4l2.c
uvc_ctrl.c uvc_entity.c uvc_queue.c uvc_video.c

Let's make a small change to the probe function of the uvcvideo driver. A probe function is called when the driver is loaded. Let's edit uvc_driver.c:

vim drivers/media/usb/uvc/uvc_driver.c

Find the probe function by searching for _probe text by typing / in standard mode. Once you've found the probe function, add pr_info() to it and save the file. A pr_info() function writes a message to the kernel log buffer, and we can see it using dmesg.

static int uvc_probe(struct usb_interface *intf,
                     const struct usb_device_id *id)
{
        struct usb_device *udev = interface_to_usbdev(intf);
        struct uvc_device *dev;
        const struct uvc_device_info *info =
                (const struct uvc_device_info *)id->driver_info;
        int function;
        int ret;

        pr_info("I changed uvcvideo driver in the Linux Kernel\n");

        if (id->idVendor && id->idProduct)
                uvc_trace(UVC_TRACE_PROBE, "Probing known UVC device %s "
                                "(%04x:%04x)\n", udev->devpath, id->idVendor,
                                id->idProduct);
        else
                uvc_trace(UVC_TRACE_PROBE, "Probing generic UVC device %s\n",
                                udev->devpath);​

Recompile your kernel, install, and reboot the system into the newly installed kernel. If you built uvcvideo as a module, after the kernel boots, you will have to load the module by running:

sudo modprobe uvcvideo

Once you load the module, let’s check if you see your message. Run:

dmesg | less

and search for “I changed”. Do you see the message?


# Practicing Commits
Let's practice committing a change. You can see the files you modified by running the git status command. Let's first check if your changes follow the coding guidelines outlined in the Linux kernel coding style guide. You can run checkpatch.pl on the diff or the generated patch to verify if your changes comply with the coding style. It is good practice to check by running checkpatch.pl on the diff before testing and committing the changes. I find it useful to do this step even before I start testing my patch. This helps avoid redoing testing in case code changes are necessary to address the checkpatch errors and warnings.

You can see my patch workflow below.


Patch Workflow

Make sure you address checkpatch errors and/or warnings. Once checkpatch is happy, test your changes and commit your changes.

If you want to commit all modified files, run:

git commit -a

If you have changes that belong in separate patches, run:

git commit <filenames>

When you commit a patch, you will have to describe what the patch does. The commit message has a subject or short log and longer commit message. It is important to learn what should be in the commit log and what doesn’t make sense. Including what code does isn’t very helpful, whereas why the code change is needed is valuable. Please read How to Write a Git Commit Message for tips on writing good commit messages.

Now, run the commit and add a commit message. After committing the change, generate the patch running the following command:

git format-patch -1 <commit ID>


## sending patches

# Sending a Patch for Review
So far, you learned how to make a change, check for coding style compliance, and generate a patch. The next step is learning the logistics of how to send a patch to the Linux Kernel mailing lists for review. The get_maintainer.pl script tells you whom to send the patch to. The two example runs of get_maintainer.pl show the list of people to send patches to. You should send the patch to maintainers, commit signers, supporters, and all the mailing lists shown in the get_maintainer.pl’s o​​​​​​​utput. Mailing lists are on the “cc” and the rest are on the “To” list when a patch is sent.

-------------------------------------------------------------------------------------------------------------------------------------
scripts/get_maintainer.pl drivers/usb/usbip/usbip_common.c
Valentina Manea <valentina.manea.m@gmail.com> (maintainer:USB OVER IP DRIVER)
Shuah Khan <shuah@kernel.org> (maintainer:USB OVER IP DRIVER)
Greg Kroah-Hartman <gregkh@linuxfoundation.org> (supporter:USB SUBSYSTEM)
linux-usb@vger.kernel.org (open list:USB OVER IP DRIVER)
-------------------------------------------------------------------------------------------------------------------------------------
scripts/get_maintainer.pl drivers/media/usb/au0828/au0828-core.c
Mauro Carvalho Chehab <mchehab@kernel.org> (maintainer:MEDIA INPUT INFRASTRUCTURE (V4L/DVB),commit_signer:7/8=88%,authored:2/8=25%,removed_lines:6/72=8%)
Hans Verkuil <hverkuil-cisco@xs4all.nl> (commit_signer:5/8=62%)
Shuah Khan <shuah@kernel.org> (commit_signer:2/8=25%,authored:2/8=25%,added_lines:150/167=90%,removed_lines:46/72=64%)
Brad Love <brad@nextdimension.cc> (commit_signer:2/8=25%,authored:2/8=25%)
Richard Fontana <rfontana@redhat.com> (commit_signer:1/8=12%)
Sean Young <sean@mess.org> (authored:1/8=12%,removed_lines:6/72=8%)
Thomas Gleixner <tglx@linutronix.de> (authored:1/8=12%,removed_lines:11/72=15%)
linux-media@vger.kernel.org (open list:MEDIA INPUT INFRASTRUCTURE (V4L/DVB))
linux-kernel@vger.kernel.org (open list)
-------------------------------------------------------------------------------------------------------------------------------------


Now let’s run the get_maintainer.pl script on your changes.​

------------------------------------------------------------------------------------------------------------------------------------
scripts/get_maintainer.pl drivers/media/usb/uvc/uvc_driver.c
Laurent Pinchart <laurent.pinchart@ideasonboard.com> (maintainer:USB VIDEO CLASS)
Mauro Carvalho Chehab <mchehab@kernel.org> (maintainer:MEDIA INPUT INFRASTRUCTURE (V4L/DVB))
linux-media@vger.kernel.org (open list:USB VIDEO CLASS)
linux-kernel@vger.kernel.org (open list)
------------------------------------------------------------------------------------------------------------------------------------

At this time, you can run:

git format-patch -1 <commit ID> --to=laurent.pinchart@ideasonboard.com --to=mchehab@kernel.org --cc=linux-media@vger.kernel.org --cc=linux-kernel@vger.kernel.org

This will generate a patch. You can send this patch using:

git send-email <patch_file>

You won’t be sending this patch and you can revert this commit.

Please refer to the Select the recipients for your patch section in the Submitting patches: the essential guide to getting your code into the kernel document.



# The Review Process
Your patch will get comments from reviewers with suggestions for improvements and, in some cases, learning to know more about the change itself. Please be patient and wait for a minimum of one week before requesting for response. During merge windows and other busy times, it might take longer than a week to get a response. Also, make sure you sent the patch to the right recipients.

Please thank the reviewers for their comments and address them. Don’t hesitate to ask a clarifying question if you don’t understand the comment. When you send a new version of your patch, add version history describing the changes made in the new version. The right place for the version history is after the "---" below the Signed-off-by tag and the start of the changed file list, as shown in the screenshot below. Everything between the Signed-off-by and the diff is just for the reviewers, and will not be included in the commit. Please don’t include version history in the commit log.


# Best Practices for Sending Patches
A few tips and best practices for sending patches:

* Run scripts/checkpatch.pl before sending the patch. Note that checkpatch.pl might suggest changes that are unnecessary! Use your best judgement when deciding whether it makes sense to make the change checkpatch.pl suggests. The end goal is for the code to be more readable. If checkpatch.pl suggests a change and you think the end result is not more readable, don't make the change. For example, if a line is 81 characters long, but breaking it makes the resulting code look ugly, don't break that line.

* Compile and test - Documentation compile steps have been documented.

* Signed-off-by should be the last tag.

* As a general rule, don't include change lines in the commit log.

* Remember that good patches get accepted quicker. It is important to understand how to create good patches.

* Copy mailing lists and maintainers/developers suggested by scripts/get_maintainer.pl.

* Be patient and wait for a minimum of one week before requesting for comments. It could take longer than a week during busy periods such as the merge windows.

* Always thank the reviewers for their feedback and address them.

* Don’t hesitate to ask a clarifying question if you don’t understand the comment.

* When working on a patch based on a suggested idea, make sure to give credit using the Suggested-by tag. Other tags used for giving credit are Tested-by, Reported-by.

* Remember that the reviewers help improve code. Don’t take it personally and handle the feedback gracefully. Please don’t do top post when responding to emails. Responses should be inlined.

* Keep in mind that the community doesn’t have any obligation to accept your patch. Patches are pulled, not pushed. Always give a reason for the maintainer to take your patch.

* Be patient and be ready to make changes and working with the reviewers. It could take multiple versions before your patch gets accepted. It is okay to disagree with maintainers and reviewers. Please don't ignore a review because you disagree with it. Present your reasons for disagreeing, along with supporting technical data such as benchmarks and other improvements.

* In general, getting response and comments is a good sign that the community likes the patch and wants to see it improved. Silence is what you want to be concerned about. If you don't hear any response back from the maintainer after a week, feel free to either send the patch again, or send a gentle "ping" - something like "Hi, I know you are busy, but have you found time to look at my patch?"

* Expect to receive comments and feedback at any time during the review process.

* Stay engaged and be ready to fix problems, if any, after the patch gets accepted into linux-next for integration into the mainline. Kernel build and Continuous Integration (CI) bots and rings usually find problems.

* When a patch gets accepted, you will either see an email from the maintainer or an automated patch accepted email with information on which tree it has been applied to, and some estimate on when you can expect to see it in the mainline kernel. Not all maintainers might send an email when the patch gets merged. The patch could stay in linux-next for integration until the next merge window, before it gets into Linus's tree. Unless the patch is an actual fix to a bug in Linus's tree, in which case, it may go directly into his tree.

* Sometimes you need to send multiple related patches. This is useful for grouping, say, to group driver clean up patches for one particular driver into a set, or grouping patches that are part of a new feature into one set. git format-patch -2 -s --cover-letter --thread --subject-prefix="PATCH v3" --to= “name” --cc=” name” will create a threaded patch series that includes the top two commits and generated cover letter template. It is a good practice to send a cover letter when sending a patch series.
* Including patch series version history in the cover letter will help reviewers get a quick snapshot of changes from version to version.

* When a maintainer accepts a patch, the maintainer assumes maintenance responsibility for that patch. As a result, maintainers have decision power on how to manage patch flow into their individual sub-system(s) and they also have individual preferences. Be prepared for maintainer-to-maintainer differences in commit log content and sub-system specific coding styles.



# Git Post-Commit Hooks
Git includes some hooks for scripts that can be run before or after specific git commands are executed. Checking the patch for compliance and errors can be automated using git pre-commit and post-commit hooks. The post-commit hook is run after you make a git commit with the git commit command.

If you don't already have /usr/share/codespell/dictionary.txt, do:

sudo apt-get install codespell

If you already have a .git/hooks/post-commit file, move it to .git/hooks/post-commit.sample. git will not execute files with the .sample extension. Then, edit the .git/hooks/post-commit file to contain only the following two lines:

#!bash
#!/bin/sh
exec git show --format=email HEAD | ./scripts/checkpatch.pl --strict --codespell
# Make sure the file is executable:
chmod a+x .git/hooks/post-commit

After you make the commit, this hook will output any checkpatch errors or warnings that your patch creates. If you see warnings or errors that you know you added, you can amend the commit by changing the file, using git add to add the changes, and then using git commit -amend to commit the changes.


































































































