// SPDX-License-Identifier: GPL-2.0
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/list.h>
#include <linux/string.h>
#include <linux/slab.h>

MODULE_DESCRIPTION("Task 13: Kernel linked list with slab cache.");
MODULE_AUTHOR("Anmol Karn <anmol.karan123@gmail.com>");
MODULE_LICENSE("GPL v2");

#define CHAR_LEN 20
struct identity {
	char name[CHAR_LEN];
	int id;
	bool busy;
	struct list_head mylist;
};

static struct kmem_cache *new_cache;
static LIST_HEAD(person_list);

static int identity_create(char *name, int id)
{
	struct identity *person;

	person = kmem_cache_alloc(new_cache, GFP_KERNEL);
	if (!person)
		return -EINVAL;
	strncpy(person->name, name, CHAR_LEN);
	person->name[CHAR_LEN - 1] = '\0';
	person->id = id;
	person->busy = 0;
	list_add(&(person->mylist), &person_list);

	pr_debug("Created identity: (%s,%d)", name, id);
	return 0;
}

static struct identity *identity_find(int id)
{
	struct identity *entry = NULL;

	list_for_each_entry(entry, &person_list, mylist) {
		if (entry->id == id)
			return entry;
	}
	return NULL;
}

static void identity_destroy(int id)
{
	struct identity *entry = identity_find(id);

	if (entry) {
		pr_debug("Destroying identity (%s, %d)", entry->name, entry->id);
		list_del(&(entry->mylist));
		kmem_cache_free(new_cache, entry);
	}
}

static int load_method(void)
{
	struct identity *temp;
	int res = 0;
	/* creation of cache */
	new_cache = kmem_cache_create("anmolcache",
					   sizeof(struct identity),
					   0, SLAB_POISON, NULL); /* here, SLAB_POISON is used to make extra entry for anmolcache in /proc/slabinfo */
	if (new_cache == NULL)
		return -ENOMEM;

	res = identity_create("Alice", 1);
	if (res)
		goto esc;
	res = identity_create("Bob", 2);
	if (res)
		goto esc;
	res = identity_create("Dave", 3);
	if (res)
		goto esc;
	res = identity_create("Gena", 10);
	if (res)
		goto esc;

	temp = identity_find(3);
	pr_debug("id 3 = %s\n", temp->name);

	temp = identity_find(42);
	if (temp == NULL)
		pr_debug("id 42 not found\n");

	identity_destroy(2);
	identity_destroy(1);
	identity_destroy(10);
	identity_destroy(42);
	identity_destroy(3);

esc:	return res;
	pr_debug("Hello, World  kernel :)\n");
	return 0;
}

static void unload_method(void)
{
	pr_debug("unloading...\n");
	/* Destroying cache */
	if (new_cache != NULL)
		kmem_cache_destroy(new_cache);
}

module_init(load_method);
module_exit(unload_method);
