static char *sysfs_id = "783dbec8fa0b";

static ssize_t id_show(struct device *dev, struct device_attribute *attr,
		       char *buf)
{
	return sprintf(buf, "%s\n", sysfs_id);
}

static ssize_t id_store(struct device *dev, struct device_attribute *attr,
			const char *buf, size_t len)
{
	if (len - 1 != strlen(sysfs_id)
	    || strncmp(buf, sysfs_id, strlen(sysfs_id)))
		return -EINVAL;
	return len;
}
static const DEVICE_ATTR_RW(id);

remove and add:
	err = device_create_file(&udev->dev, &dev_attr_id);
	if (err)
		return -ENOMEM;

	device_remove_file(&intf->dev, &dev_attr_id);
    
//    drivers/usb/core/sysfs.c
//    /sys/devices/pci0000:00/0000:00:14.0/usb1/uevent
